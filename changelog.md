# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1-8-0] - 2020-05-05

### Added

**Features**

[#19207] init parameter to set max zoom level


## [1-7-0] - 2020-04-24

### Added

**Features**

[#19109] integrated with new Gis-Viewer capabilities
	
[#19111] GisViewer 'zoom' and 'center' to Map readable as HTTP GET parameters
		
[#19172] update the look and feel of Gis-Viewer


